package ru.pisarev.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    @NotNull
    public UserNotFoundException() {
        super("Error. User not found.");
    }

}
