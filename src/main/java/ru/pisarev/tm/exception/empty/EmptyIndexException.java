package ru.pisarev.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.exception.AbstractException;

public class EmptyIndexException extends AbstractException {

    @NotNull
    public EmptyIndexException() {
        super("Error. Index is empty");
    }

}
